import js2py
from requests_html import HTMLSession
import json

session = HTMLSession()
url_login = 'https://id.zalo.me/login/logininfo'
url_getKey = 'https://wpa.chat.zalo.me/api/login/getLoginInfo?' \
             'zpw_ver=414&zpw_type=30&imei=b5092081-1533-4b79-bf97-a7e3cb32d4fa-' \
             '041e452e3ad05f447d7ccadee9d67627&computer_name=Web&language=vi&ts=1618420887806&nretry=0'

cookie = '__zi=3000.SSZzejyD6zOgdh2mtnLQWYQN_RAG01ICFjIXe9fEM8uzckcWdarSZ7QRxw7QI5YEUv_agpCr.1; __zi-legacy=3000.SSZzejyD6zOgdh2mtnLQWYQN_RAG01ICFjIXe9fEM8uzckcWdarSZ7QRxw7QI5YEUv_agpCr.1; _ga=GA1.2.1819551806.1615218442; fpsend=149854; _ga=GA1.3.1819551806.1615218442; _zlang=vn; _gid=GA1.2.471495226.1618419743; app.event.zalo.me=329477379753294619; _gid=GA1.3.471495226.1618419743; zpsid=Udlr.233427000.71.GCFTEy97dmtwbtO0paUV3RSqxJ_nTAi_zNUlFUWs48jTLUUHmvAnUg97dmq; zpsidleg=Udlr.233427000.71.GCFTEy97dmtwbtO0paUV3RSqxJ_nTAi_zNUlFUWs48jTLUUHmvAnUg97dmq; zpw_sek=Hu70.233427000.a0.o_zFAFxhext6Eqgmt-kZlON9njRSqO7KbPxL-9gXyFYMfjNkjwJdpgkJYEw9rfwVWy7ICdX6QNihwdMJYOl6PG; zpw_sekm=7fto.233427000.71.YynpdlkefkB3U4_lzgYcuexRmv39cvNMoP-Mqik2rsrz1MmaBgbmp8p8fk8; _gat=1'
url_profile = 'https://friend-wpa.chat.zalo.me/api/friend/profile/get?zpw_ver=414&zpw_type=30&params='


def auto_login():
    r = session.get(
        url_getKey,
        headers={
            'Accept': 'application/json, text/plain, */*',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'en-US,en;q=0.5',
            'Connection': 'keep-alive',
            'Cookie': cookie,
            'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:87.0) Gecko/20100101 Firefox/87.0',
            'TE': 'Trailers',
            'Referer': 'https://chat.zalo.me/',
            'Host': 'wpa.chat.zalo.me',
            'Origin': 'https://chat.zalo.me',
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    )
    data_out = json.loads(r.text)
    key = data_out['data']['zpw_enk']
    return key

def get_profile(phones: str) -> str:
    urls = url_profile+phones
    print(urls)
    r = session.get(
        urls,
        headers={
            'Accept': 'application/json, text/plain, */*',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'en-US,en;q=0.5',
            'Connection': 'keep-alive',
            'Cookie': cookie,
            'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:87.0) Gecko/20100101 Firefox/87.0',
            'TE': 'Trailers',
            'Referer': 'https://chat.zalo.me/',
            'Host': 'friend-wpa.chat.zalo.me',
            'Origin': 'https://chat.zalo.me',
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    )
    data_out = json.loads(r.text)
    key = data_out['data']
    return key


def encode_aes(secret_key: str, text: str) -> str:
    """
    Encode Zalo's params
    :param secret_key: Secret key
    :param text: Text that need to encode
    :return: Encoded text
    """
    CryptoJS = js2py.require('crypto-js')
    return CryptoJS.AES.encrypt(text, CryptoJS.enc.Base64.parse(secret_key), {
        "iv": CryptoJS.enc.Hex.parse("00000000000000000000000000000000"),
        "mode": CryptoJS.mode.CBC,
        "padding": CryptoJS.pad.Pkcs7
    }).ciphertext.toString(CryptoJS.enc.Base64)


def decode_aes(secret_key, text) -> str:
    CryptoJS = js2py.require('crypto-js')
    decodeURIComponent = js2py.eval_js("decodeURIComponent")
    return CryptoJS.AES.decrypt({
        "ciphertext": CryptoJS.enc.Base64.parse(decodeURIComponent(text)),
        "salt": ""
    }, CryptoJS.enc.Base64.parse(secret_key), {
        "iv": CryptoJS.enc.Hex.parse("00000000000000000000000000000000"),
        "mode": CryptoJS.mode.CBC,
        "padding": CryptoJS.pad.Pkcs7
    }).toString(CryptoJS.enc.Utf8)


def list_to_string(s):
    # initialize an empty string
    str1 = ""

    # traverse in the string
    for ele in s:
        str1 += str(ele)

        # return string
    return str1


def convert_phone(phone_num: str) -> str:
    temp = phone_num
    count = len(phone_num)
    if count == 10:
        list_num = [int(i) for i in str(temp)]
        list_num.remove(0)
        phone_out = '+84' + list_to_string(list_num)
        return phone_out
    else:
        return temp


if __name__ == '__main__':
    print('Enter phone:')
    phone_input = input()
    phone_push = convert_phone(phone_input)
    key_crypto = auto_login()
    value = '"' + phone_push + '"'
    phone = encode_aes(secret_key=key_crypto, text='{"phone":%s,"avatar_size":240}' % value)
    # a = 'FL%2BsZcwnQocW77LmurDxhmKayJhsiD8csAajVetmRjDgTx4KKxL6mMCmsdr54f8Q'
    data = get_profile(phones=phone)
    profile = decode_aes(secret_key=key_crypto, text=data)
    print(profile)
